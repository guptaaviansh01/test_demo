import unittest
import fun2.lambda_function as lambda_function
class TestHandlerCase(unittest.TestCase):

    def test_result(self):
        print("testing result.")
        result = lambda_function.lambda_handler(3, 2)
        print(result)
        self.assertEqual(result, 5)
        event={'Country':'INDIA'}
        result = lambda_function.lambda_handler(2, -9)
        print(result)
        self.assertEqual(result, -7)


if __name__ == '__main__':
    unittest.main()